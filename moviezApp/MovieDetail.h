//
//  MovieDetail.h
//  moviezApp
//
//  Created by Mattias Karlsson on 2015-04-25.
//  Copyright (c) 2015 Mattias Karlsson. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface MovieDetail : UIViewController
@property NSDictionary *films;
-(void)populateLabels;
-(void)fetchImage;
@property (nonatomic) BOOL isfav;
@property (nonatomic) NSInteger index;
@property (weak, nonatomic) IBOutlet UIImageView *imageLabel;
@end
