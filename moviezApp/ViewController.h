//
//  ViewController.h
//  moviezApp
//
//  Created by Mattias Karlsson on 2015-04-19.
//  Copyright (c) 2015 Mattias Karlsson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchHandler.h"
#import "MovieTable.h"

@interface ViewController : UIViewController


@end

