//
//  MovieTableCell.m
//  moviezApp
//
//  Created by Mattias Karlsson on 2015-04-25.
//  Copyright (c) 2015 Mattias Karlsson. All rights reserved.
//

#import "MovieTableCell.h"

@implementation MovieTableCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
