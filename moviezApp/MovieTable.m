//
//  MovieTable.m
//  moviezApp
//
//  Created by Mattias Karlsson on 2015-04-25.
//  Copyright (c) 2015 Mattias Karlsson. All rights reserved.
//

#import "MovieTable.h"
#import "SearchHandler.h"
#import "MovieTableCell.h"


@interface MovieTable ()

@property (nonatomic) SearchHandler *handler;
@property (nonatomic) int movieId;
@end

@implementation MovieTable

- (void)viewDidLoad {
    [super viewDidLoad];
    self.handler = [[SearchHandler alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)reloadTable{
    [self.tableView reloadData];
}

#pragma mark - Table view data source


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return self.films.count;
}
//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MovieTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyCell" forIndexPath:indexPath];
    
    //så fort jag gör outlets till dessa två labels så kraschar appen, har försökt med mängder olika saker
    //cell.movieTitle.text = self.films[indexPath.row][@"title"];
    //cell.voteAverage.text = self.films[indexPath.row][@"vote_average"];
    NSLog(@"titel%@",self.films[indexPath.row][@"title"]);
    NSLog(@"rating%@",self.films[indexPath.row][@"vote_average"]);
    NSString * imagePath =  self.films[indexPath.row][@"poster_path"];
    NSLog(@"sökväg till bild%@",imagePath);
    if (imagePath != nil) {
        [self.handler fetchTableImages:(NSString*)imagePath andCell:(MovieTableCell*)cell];
    }
    
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
   
    NSLog(@"HHHHHHHHHH  %d",self.movieId);
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    MovieDetail *detailView = [segue destinationViewController];
    UITableViewCell *cell = sender;
    NSIndexPath *path = [self.tableView indexPathForCell:cell];
    
    NSDictionary *dict = self.films[path.row];
    for (id key in dict)
    {
        id value = [dict objectForKey:key];
        if ([key isEqual: @"id"]) {
            NSLog(@"%@",value);
            self.movieId = (int)[value integerValue];
        }
        if([key isEqual: @"isFav"]) {
            NSLog(@"%@",value);
            detailView.isfav = YES;
            detailView.index = path.row;
        }

    }
    [self.handler findDetails:detailView andMovieId:self.movieId];
}


@end
