//
//  SearchHandler.h
//  moviezApp
//
//  Created by Mattias Karlsson on 2015-04-25.
//  Copyright (c) 2015 Mattias Karlsson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MovieTable.h"
#import "MovieTableCell.h"
#import "MovieDetail.h"

@interface SearchHandler : NSObject
-(void)findDetails:(MovieDetail*)view andMovieId:(int)movieId;
-(void)findMovies:(MovieTable*)view andPages:(int)pages andOrig:(NSString*)from;
-(void)fetchTableImages:(NSString*)imagePath andCell:(MovieTableCell*)cell;
-(void)fetchDetailImages:(NSString*)imagePath andImageView:(MovieDetail*)view;

@end
