//
//  SearchHandler.m
//  moviezApp
//
//  Created by Mattias Karlsson on 2015-04-25.
//  Copyright (c) 2015 Mattias Karlsson. All rights reserved.
//

#import "SearchHandler.h"
@interface SearchHandler()
@end
@implementation SearchHandler

-(void)fetchTableImages:(NSString*)imagePath andCell:(MovieTableCell*)cell {
    __block NSString *search = [NSString stringWithFormat:@"http://image.tmdb.org/t/p/w154/%@?api_key=2be566cce106408c7bd401e9e2468f3c",imagePath];
    NSURL *URL = [NSURL URLWithString:search];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      
                                      if (error) {
                                          NSLog(@"error in response: %@",error);
                                          return;
                                      }
                                      
                                      if ([response isKindOfClass:[NSHTTPURLResponse class]]) {
                                          NSLog(@"Response HTTP Status code: %ld\n", (long)[(NSHTTPURLResponse *)response statusCode]);
                                          NSLog(@"Response HTTP Headers:\n%@\n", [(NSHTTPURLResponse *)response allHeaderFields]);
                                      }
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          UIImage *image = [[UIImage alloc] initWithData:data];
                                         
                                          cell.cellImage.image = image;
                                          [cell updateConstraintsIfNeeded];
                                          //[cell.cellImage reloadInputViews];
                                        });
                                      
                                  
                                  }];
    [task resume];
}

-(void)fetchDetailImages:(NSString*)imagePath andImageView:(MovieDetail*)view {
    __block NSString *search = [NSString stringWithFormat:@"http://image.tmdb.org/t/p/w154/%@?api_key=2be566cce106408c7bd401e9e2468f3c",imagePath];
    NSURL *URL = [NSURL URLWithString:search];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];

    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                        completionHandler:
                              ^(NSData *data, NSURLResponse *response, NSError *error) {
                                  
                                  if (error) {
                                      NSLog(@"error in response: %@",error);
                                      return;
                                  }
                                  
                                  if ([response isKindOfClass:[NSHTTPURLResponse class]]) {
                                      NSLog(@"Response HTTP Status code: %ld\n", (long)[(NSHTTPURLResponse *)response statusCode]);
                                      NSLog(@"Response HTTP Headers:\n%@\n", [(NSHTTPURLResponse *)response allHeaderFields]);
                                  }
                                  dispatch_async(dispatch_get_main_queue(), ^{
                                      UIImage *image = [[UIImage alloc] initWithData:data];
                                      NSLog(@"%@",data);
                                      view.imageLabel.image = image;
                                      [view reloadInputViews];

                                      //får inte fram bilden
                                  });
                                  
                                  
                              }];
[task resume];
}

-(void)findDetails:(MovieDetail*)view andMovieId:(int)movieId {
    NSString *search = [NSString stringWithFormat:@"http://api.themoviedb.org/3/movie/%d?api_key=2be566cce106408c7bd401e9e2468f3c", movieId];
    NSLog(@"ididididi%d",movieId);
    NSURL *URL = [NSURL URLWithString:search];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //[request setValue:2be566cce106408c7bd401e9e2468f3c forHTTPHeaderField:@"api_key"];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      
                                      if (error) {
                                          NSLog(@"error in response: %@",error);
                                          return;
                                      }
                                      
                                      if ([response isKindOfClass:[NSHTTPURLResponse class]]) {
                                          NSLog(@"Response HTTP Status code: %ld\n", (long)[(NSHTTPURLResponse *)response statusCode]);
                                          NSLog(@"Response HTTP Headers:\n%@\n", [(NSHTTPURLResponse *)response allHeaderFields]);
                                      }
                                      NSError *parsingError = nil;
                                      NSDictionary *root = [NSJSONSerialization JSONObjectWithData:data
                                                                                           options:kNilOptions error:&parsingError];
                                      if(!parsingError) {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              if (root.count>0) {
                                                  NSLog(@"film details %@", root);
                                                  view.films = root;
                                                  [view populateLabels];
                                              }
                                          });
                                      }
                                      else {
                                          NSLog(@"couldn't parse parsinerror: %@", parsingError);
                                      }
                                  }];
    [task resume];
}


-(void)findMovies:(MovieTable*)view andPages:(int)pages andOrig:(NSString*)from {
    
    NSString *search;
    if ([from  isEqualToString: @"popular"]) {
        search = [NSString stringWithFormat:@"http://api.themoviedb.org/3/movie/top_rated?api_key=2be566cce106408c7bd401e9e2468f3c&page=%d", pages];
    }
    else {
        
        search = [NSString stringWithFormat:@"http://api.themoviedb.org/3/search/multi?api_key=2be566cce106408c7bd401e9e2468f3c&query=%@&page=%d",from, pages];
    }
    
    NSURL *URL = [NSURL URLWithString:search];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //[request setValue:2be566cce106408c7bd401e9e2468f3c forHTTPHeaderField:@"api_key"];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      
                                      if (error) {
                                          NSLog(@"error in response: %@",error);
                                          return;
                                      }
                                      
                                      if ([response isKindOfClass:[NSHTTPURLResponse class]]) {
                                          NSLog(@"Response HTTP Status code: %ld\n", (long)[(NSHTTPURLResponse *)response statusCode]);
                                          NSLog(@"Response HTTP Headers:\n%@\n", [(NSHTTPURLResponse *)response allHeaderFields]);
                                      }
                                      NSError *parsingError = nil;
                                      NSDictionary *root = [NSJSONSerialization JSONObjectWithData:data
                                                                                        options:kNilOptions error:&parsingError];
                                      if(!parsingError) {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              if (root.count>0) {
                                                  NSLog(@"best film %@", root);
                                                  view.films=root[@"results"];
                                                  [view reloadTable];
                                              }
                                          });
                                      }
                                      else {
                                          NSLog(@"couldn't parse parsinerror: %@", parsingError);
                                      }
                                  }];
    [task resume];
}


@end