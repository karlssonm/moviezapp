//
//  MovieTable.h
//  moviezApp
//
//  Created by Mattias Karlsson on 2015-04-25.
//  Copyright (c) 2015 Mattias Karlsson. All rights reserved.
//

#import <UIKit/UIKit.h>


#import "MovieDetail.h"


@interface MovieTable : UITableViewController
@property (nonatomic) NSArray *films;
-(void)reloadTable;
@end
