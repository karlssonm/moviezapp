//
//  MovieDetail.m
//  moviezApp
//
//  Created by Mattias Karlsson on 2015-04-25.
//  Copyright (c) 2015 Mattias Karlsson. All rights reserved.
//

#import "MovieDetail.h"
#import "SearchHandler.h"

@interface MovieDetail ()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
//@property (weak, nonatomic) IBOutlet UIImageView *imageLabel;
@property (weak, nonatomic) IBOutlet UILabel *ratingLabel;

@property (weak, nonatomic) IBOutlet UILabel *releaseLabel;
@property (weak, nonatomic) IBOutlet UITextView *plotLabel;
@property (weak, nonatomic) IBOutlet UILabel *runtimeLabel;
@property (weak, nonatomic) IBOutlet UITextView *genreLabel;
@property (nonatomic) SearchHandler *handler;
@property (nonatomic) NSUserDefaults *defaults;
@property (weak, nonatomic) IBOutlet UIButton *favbutton;
@end

@implementation MovieDetail
- (IBAction)savToFav:(id)sender {
    NSMutableArray *arr = [NSMutableArray arrayWithArray:[self.defaults arrayForKey:@"favArray"]];
    if(self.isfav){
        [arr removeObjectAtIndex:self.index];
        self.isfav = NO;
        [self.favbutton setTitle:@"Add Favourte" forState:UIControlStateNormal];
    }
    else {
        NSDictionary *saveArr = @{@"id":self.films[@"id"],
                              @"title":self.films[@"title"],
                              @"vote_average":self.films[@"vote_average"],
                              @"poster_path":self.films[@"poster_path"],
                              @"isFav": [NSNumber numberWithBool:self.isfav]};
    
    
        [arr addObject:saveArr];
        self.isfav=YES;
        [self.favbutton setTitle:@"Del Favourite" forState:UIControlStateNormal];
    }

    [self.defaults setObject:arr forKey:@"favArray"];
    [self.defaults synchronize];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    if(self.isfav){
        [self.favbutton setTitle:@"Del Favourite" forState:UIControlStateNormal];
    }
    self.handler = [[SearchHandler alloc] init];
    self.defaults = [NSUserDefaults standardUserDefaults];

    if (![self.defaults objectForKey:@"favArray"]){
        NSMutableArray *arr = [[NSMutableArray alloc] init];
        [self.defaults setObject:arr forKey:@"favArray"];
        [self.defaults synchronize];
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)populateLabels{
    __block float rating = [[self.films objectForKey:@"vote_average"] floatValue];
    __block NSArray* genres = self.films[@"genres"];
    __block NSMutableArray* genre = [[NSMutableArray alloc] init];
    for (int i=0; i<genres.count; i++){
        [genre addObject:[genres[i] objectForKey:@"name"]];
    }
    self.genreLabel.text = [genre componentsJoinedByString:@", "];
    self.titleLabel.text = self.films[@"title"];
    self.releaseLabel.text = self.films[@"release_date"];
    self.ratingLabel.text = [NSString stringWithFormat:@"%.1f",rating];
    self.plotLabel.text = self.films[@"overview"];
    self.runtimeLabel.text = [NSString stringWithFormat:@"%@ min",self.films[@"runtime"]];
    [self fetchImage];
    
}
-(void)fetchImage{
    NSString * imagePath =  self.films[@"poster_path"];

    if (imagePath != nil) {
        [self.handler fetchDetailImages:(NSString*)imagePath andImageView:self];
    }

}



@end
