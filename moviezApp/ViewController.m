//
//  ViewController.m
//  moviezApp
//
//  Created by Mattias Karlsson on 2015-04-19.
//  Copyright (c) 2015 Mattias Karlsson. All rights reserved.
//

#import "ViewController.h"


@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *logo;
@property (weak, nonatomic) IBOutlet UIImageView *zapp;
@property (nonatomic) SearchHandler *handler;
@property (nonatomic) NSUserDefaults *defaults;


@end

@implementation ViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    self.handler = [[SearchHandler alloc] init];
    self.defaults = [NSUserDefaults standardUserDefaults];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    MovieTable *tableView = [segue destinationViewController];
    if([segue.identifier isEqualToString:@"toTable"]){
        
        [self.handler findMovies:tableView andPages:1 andOrig:@"popular"];
        tableView.title = @"Popular Movies";
    }
    if([segue.identifier isEqualToString:@"favouriteTable"]){
        NSMutableArray *arr = [NSMutableArray arrayWithArray:[self.defaults arrayForKey:@"favArray"]];
        NSLog(@"arrayr%@",arr);
        tableView.films = arr;
        tableView.title = @"Favorites";
    }
}
@end
