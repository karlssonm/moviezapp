//
//  SearchView.h
//  moviezApp
//
//  Created by Mattias Karlsson on 2015-04-26.
//  Copyright (c) 2015 Mattias Karlsson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MovieTable.h"
#import "SearchHandler.h"

@interface SearchView : UIViewController

@end
