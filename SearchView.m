//
//  SearchView.m
//  moviezApp
//
//  Created by Mattias Karlsson on 2015-04-26.
//  Copyright (c) 2015 Mattias Karlsson. All rights reserved.
//

#import "SearchView.h"

@interface SearchView ()
@property (nonatomic) SearchHandler *handler;
@property (weak, nonatomic) IBOutlet UITextField *inputSearch;
@property (weak, nonatomic) IBOutlet UIButton *hiddenButton;
@property (nonatomic) NSString *searchText;
@end

@implementation SearchView
- (IBAction)gobutton:(id)sender {
    self.searchText = self.inputSearch.text;
    [self performSegueWithIdentifier:@"searchTable" sender:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.hiddenButton.hidden = YES;
    self.handler = [[SearchHandler alloc] init];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    MovieTable *tableView = [segue destinationViewController];
    if([segue.identifier isEqualToString:@"searchTable"]){
        NSLog(@"%@",self.inputSearch.text);
        [self.handler findMovies:tableView andPages:1 andOrig:self.searchText];
        tableView.title = @"Search Results";
    }

}


@end
